(function ($) {

    if (!Drupal.image_compressor) Drupal.image_compressor = {};

    Drupal.image_compressor.triggerUploadButton = function (event) {
        let count = event.target.files.length;
        let container = new DataTransfer();
        const finishedCallback = function () {
            event.target.files = container.files;
            $(event.target)
                .closest('.js-form-managed-file')
                .find('.js-form-submit[data-drupal-selector$="upload-button"]')
                .trigger('mousedown');
        }
        Array.from(event.target.files).forEach(function (file, index) {
            const options = {
                success(result) {
                    const newFile = new File([result], file.name);
                    container.items.add(newFile);
                    if (container.files.length === count) finishedCallback();
                }
            };
            if (event.target.dataset.width) {
                options.maxWidth = Number(event.target.dataset.width);
            }
            if (event.target.dataset.height) {
                options.maxHeight = Number(event.target.dataset.height);
            }
            new Compressor(file, options);
        });
    };

    Drupal.behaviors.fileAutoUpload = {
        attach: function attach(context) {
            $(once('auto-file-upload', 'input[type="file"]', context)).on('change.autoFileUpload', Drupal.image_compressor.triggerUploadButton);
        },
        detach: function detach(context, settings, trigger) {
            if (trigger === 'unload') {
                $(once.remove('auto-file-upload', 'input[type="file"]', context)).off('.autoFileUpload');
            }
        }
    }

})(jQuery);